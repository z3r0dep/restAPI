<?php

include_once '../src/autoload.php';

use App\Router;
use App\Main;

$router = new Router();
$app = new Main();

$router->get('/', function() use ($app){
    $app->index();
});

$router->mount('/api', function () use ($router, $app)
{
    $router->get('/', function() use ($app){
        $app->ping();
    });
    $router->get('/name', function() use ($app){
        $app->listName();
    });
    $router->post('/name', function() use ($app){
        $app->insertName();
    });
    $router->put('/name', function() use ($app){
        $app->updateName();
    });
    $router->delete('/name', function() use ($app){
        $app->deleteName();
    });
});

$router->run();
