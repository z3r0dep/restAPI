<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>REST test form</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en">
    <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.grey-pink.min.css" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <style>
        .mdl-card {
            width: 400px;
            min-height: 0;
        }
        .mdl-textfield {
            width: 360px;
        }
    </style>
</head>

<body>
<div aria-live="assertive" aria-atomic="true" aria-relevant="text" class="mdl-snackbar mdl-js-snackbar">
    <div class="mdl-snackbar__text"></div>
    <button type="button" class="mdl-snackbar__action"></button>
</div>

<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
    <main class="mdl-layout__content">
        <div class="mdl-grid portfolio-max-width portfolio-contact">
            <div class="mdl-cell mdl-cell--12-col mdl-card mdl-shadow--4dp">
                <div class="mdl-card__supporting-text">
                    <form>
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <input class="mdl-textfield__input" pattern="[A-Z,a-z, ]*" type="text" id="name" name="name" maxlength="100">
                            <label class="mdl-textfield__label" for="name">Nome</label>
                            <span class="mdl-textfield__error">Letras e espaços apenas</span>
                        </div>
                        <p>
                            <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" type="submit">
                                Enviar
                            </button>
                        </p>
                    </form>
                </div>
            </div>
        </div>

        <div class="mdl-grid portfolio-max-width portfolio-contact">
            <div class="mdl-cell mdl-cell--12-col mdl-card mdl-shadow--4dp">
                <div class="mdl-card__supporting-text">
                    <table class="mdl-data-table mdl-js-data-table">
                        <tbody id="data-table">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </main>
</div>
<script src="https://code.getmdl.io/1.3.0/material.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
    $(function() {
        $("form").on("submit", function(e) {
            e.preventDefault();

            if ($('form input#name').val())
            {
                $.ajax({
                    url: "/api/name",
                    method: "POST",
                    data: $(this).serialize()
                })
                .done(function(result){
                    snackBar(result.message);
                    $('form')[0].reset();

                    refreshDataTable();
                })
            }
            else {
                snackBar('digite o nome');
            }
        });

        refreshDataTable();
    });

    function snackBar (message){
        var notification = document.querySelector('.mdl-js-snackbar');
        notification.MaterialSnackbar.showSnackbar({
            message: message,
            timeout: 3000
        });
    }

    function refreshDataTable() {
        $('#data-table').html('');

        $.ajax({
            url: "/api/name",
            method: "GET",
        })
        .done(function(result){
            $.each(result, function(i, item) {
                $('#data-table').append(
                    '<tr>' +
                        '<td class="mdl-data-table__cell--non-numeric">'+item.name+' ' +
                        '</td>' +
                        '<td>' +
                            '<a href="#" class="delete icon material-icons" data-id="'+item.id+'">delete</span></a>' +
                        '</td>' +
                    '</tr>'
                );
            });

            $("a.delete").on("click", function(e) {
                e.preventDefault();

                $.ajax({
                    url: "/api/name",
                    method: "DELETE",
                    data: {id: $(this).attr("data-id")}
                })
                .done(function(result){
                    refreshDataTable();
                });
            });
        })

    }
</script>
</body>

</html>
