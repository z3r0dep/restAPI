<?php

namespace App;

class Database
{
    private $pdo;

    public function connect() {
        if ($this->pdo == null) {
            try {
                $this->pdo = new \PDO('sqlite:../data/api.db');
            } catch (\PDOException $e) {
                echo 'exception: '.$e->getMessage();
            }
        }
        return $this->pdo;
    }
}
