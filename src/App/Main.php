<?php

namespace App;

use App\Model\Api;

class Main
{
    private $api;

    public function __construct()
    {
        $this->api = new Api();
    }

    public function index(){
        $template = new Template();
        $template->load('layout.php');
    }

    public function ping() {
    }

    public function listName() {
        header('Content-Type: application/json');
        echo json_encode($this->api->listName());
    }

    public function insertName() {
        header('Content-Type: application/json');
        echo json_encode($this->api->insertName());
    }

    public function updateName() {
        header('Content-Type: application/json');
        echo json_encode($this->api->updateName());
    }

    public function deleteName() {
        header('Content-Type: application/json');
        echo json_encode($this->api->deleteName());
    }
}
