<?php

namespace App\Model;

use App\Database;

class Api
{
    private $pdo;

    public function __construct()
    {
        $this->pdo = (new Database())->connect();
    }

    public function listName(){
        $result = $this->_search();

        return $result;
    }

    public function insertName()
    {
        $name = $_REQUEST['name'];
        $result = $this->_search($name);

        if (empty($result)) {
            $insert = $this->pdo->prepare('INSERT INTO person(`name`) VALUES(:name)');
            $insert->bindValue(':name', $name);
            $insert->execute();

            return ['message' => 'nome incluído com sucesso', 'id' => $this->pdo->lastInsertId()];
        }
        else {
            return ['message' => 'nome existente'];
        }
    }

    public function updateName()
    {
        $data = '';
        parse_str(file_get_contents('php://input'), $data);

        if (!empty($data)) {
            $result = $this->_search($data['name']);

            if (empty($result)) {
                $update = $this->pdo->prepare("UPDATE person SET name = :name WHERE id = :id");
                $update->bindValue(':name', $data['name']);
                $update->bindValue(':id', $data['id']);
                $update->execute();

                return ['message' => 'ID ' . $data['id'] . ' alterado para ' . $data['name']];
            }
            else {
                return ['message' => 'nome existente'];
            }
        }
        else {
            return ['message' => 'dados invalidos'];
        }
    }

    public function deleteName(){
        $data = '';
        parse_str(file_get_contents('php://input'), $data);

        if (!empty($data)) {
            $delete = $this->pdo->prepare("DELETE FROM person WHERE id = :id");
            $delete->bindValue(':id', $data['id']);
            $delete->execute();

            return ['message' => 'ID ' . $data['id'] . ' excluído'];
        }
        else {
            return ['message' => 'dados invalidos'];
        }
    }

    private function _search($name = null) {
        $where = ($name != null) ? " WHERE name = '".$name."'" : '';
        $query = $this->pdo->query('SELECT * FROM person'.$where);

        $result = [];
        while ($row = $query->fetch(\PDO::FETCH_ASSOC)) {
            $result[] = [
                'id' => $row['id'],
                'name' => $row['name']
            ];
        }

        return $result;
    }
}
