<?php

namespace App;

class Template
{
    public function load($template)
    {
        define('TEMPLATE_DIR', __DIR__.'/../');

        if (file_exists(TEMPLATE_DIR.$template)) {
            ob_start();
            require_once(TEMPLATE_DIR.$template);
            $template = ob_get_contents();
            ob_end_clean();
            echo $template;
        }
        else {
            return false;
        }
    }
}
